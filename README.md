# README
SeQura provides ecommerce shops (merchants) a flexible payment method so their customers (shoppers) can purchase and receive goods without paying upfront. SeQura earns a small fee per purchase and pays out (disburse) the merchant once the order is marked as completed.

The operations manager is now asking you to make a system to calculate how much money should be disbursed to each merchant

## How to run this solution
* Clone this repo
* Install all necessary libraries (gems) with Bundler `bundle install`
* In the project folder, set up the database with `rails db:setup`
* Run the server with `rails s`

For the background jobs, Redis server needs to be installed in the current machine.
* Linux-based: `apt install redis-server`
* Mac (with Homebrew): `brew install redis`

The Disbursment scheduler needs to be loaded in the crontab using the gem 'whenever', so that the disbursements can be
calculated automatically every monday at 7am. For this, you need to:

* Update your crontab with `bundle exec whenever --update-crontab`
* Check the runner has been scheduled successfully: `crontab -l`

If you don't want to wait to next Monday at 7am for automatic disburse, you can enqueue the job manually with:
`rails disbursements:run`

To execute pending jobs:
`bundle exec sidekiq -e development`  
Sidekiq runner will start in the console (foreground), use `Ctrl + C` to kill

#### To retrieve the disbursements via API
* specific merchant: `localhost:3000/merchant_disbursements?merchant=[merchant_id]`
* all merchants: `localhost:3000/merchant_disbursements`
* only disbursements for a given week: `localhost:3000/merchant_disbursements?week=52/2019`

#### Tests
Tests can be run with `rspec spec`

### Technical choices
#### Ruby/Rails
As the API framework  
Pros: Quick prototyping and robust environment.  
Cons: Very opinionated. Have to follow the "Rails Way"

#### Postgresql
As the DBMS  
Pros: Familiarity, Powerful, suitable for the needs of this solution.  
Cons: Not a lightweight option.

#### Sidekiq
As background job runner  
Pros: Efficient, easy to setup and manage, great interface for information and administratino  
Cons: Depends on Redis to store and execute jobs.

#### Whenever
As processes scheduler  
Pros: Easy to use  
Cons: Needs to modify system's crontab
