class ApplicationController < ActionController::API

  # render any error message as JSON object
  rescue_from StandardError do |e|
    render json: { message: e.message }, status: :unprocessable_entity
  end

end
