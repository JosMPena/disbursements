class MerchantDisbursementsController < ApplicationController
  before_action :filter_params
  before_action :search_collection

  def index
    # looking forward: it would be a good idea to add pagination to @collection
    if @collection&.any?
      render json: @collection.to_json, status: :ok
    else
      render json: { message: 'nothing found' }, status: :not_found
    end
  end

  private

  def filter_params
    week = params[:week]
    return unless week

    if week.match? /\d{2}\/\d{4}/
      @week = week.split('/').map(&:to_i)
    else
      raise "Unsupported value for week: #{week}. Correct format is WW/YYYY"
    end

  end

  def search_collection
    @collection = Disbursement.search(
      merchant: params[:merchant],
      week: @week
    )
  end
end
