# Creates instances of disbursements to keep track of what's
# being paid to Merchants
class Disbursement < ApplicationRecord
  belongs_to :merchant

  scope :by_week, lambda { |week, year|
    monday = Date.commercial(year.to_i, week.to_i, 1)
    sunday = monday.end_of_week
    where(disbursed_on: monday..sunday)
  }

  def create_with(merchant:, amount:)
    self.class.create!(
      merchant_id: merchant,
      disbursed_amount: calc_disbursement_for(amount),
      disbursed_on: Time.zone.now,
      fee_amount: calc_fee_for(amount)
    )
  end

  class << self
    def search(merchant: nil, week: nil)
      scope = Disbursement.all
      scope = scope.where(merchant_id: merchant) if merchant
      scope = scope.by_week(*week) if week
      scope
    end
  end

  private

  def calc_disbursement_for(amount)
    # Calculates disbursement for an specific amount
    # by subtracting the fee

    # guard in case the amount is nil or negative
    raise_unsupported_amount(amount) unless amount.positive?
    amount - calc_fee_for(amount)
  end

  def calc_fee_for(amount)
    # leaving this method as public as this could be a useful calculation
    # outside this class and also for unit testing
    multiplier = case amount
                 when 0.00..49.99
                   0.01
                 when 50.00..300.00
                   0.0095
                 when proc { |n| n > 300 }
                   0.0085
                 else
                   raise_unsupported_amount(amount)
                 end

    amount * multiplier
  end

  def raise_unsupported_amount(amount)
    # Keep error message in one place
    raise "#{amount} is not a supported value"
  end
end
