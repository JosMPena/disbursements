# Proceses disbursements as a background job using Sidekiq as
# queuing backend
class DisbursementsWorker
  include Sidekiq::Worker

  def perform
    Disburser.new.disburse_merchants
  end
end
