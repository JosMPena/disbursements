# Configuration
redis = Hash.new do |h, k|
  h[k] = Hash.new(url: ENV['REDIS_SERVER_URL'].presence || 'redis://localhost:6379')
end

redis['development'] = { url: 'redis://localhost:6390' }
redis['test'] = { url: 'redis://localhost:6391' }

# Parse the env-specific url
uri = URI.parse(redis.dig(Rails.env, :url))

# Boot local redis in dev
if Rails.env.development?
  system("redis-server --port #{uri.port} --daemonize yes")
  raise "Couldn't start redis" if $CHILD_STATUS.exitstatus != 0
end

# Initialize application-wide constant. NECESSARY
REDIS = Redis.new(url: uri.to_s, port: uri.port).freeze
puts ">> Initialized REDIS with #{REDIS.inspect}"
