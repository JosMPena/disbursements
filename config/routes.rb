Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'merchant_disbursements', to: 'merchant_disbursements#index'
end
