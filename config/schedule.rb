# output logs to cron specific file
set :output, 'log/cron.log'

every :monday, at: '7:00 am' do
  runner 'DisbursementsWorker.perform_async'
end
