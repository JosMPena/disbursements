class CreateDisbursements < ActiveRecord::Migration[6.0]
  def change
    create_table :disbursements do |t|
      t.references :merchant, null: false, foreign_key: true
      t.float :disbursed_amount
      t.float :fee_amount
      t.timestamp :disbursed_on

      t.timestamps
    end
  end
end
