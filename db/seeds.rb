%w[merchants shoppers orders].each do |filename|
  klass = filename.singularize.capitalize.constantize
  # read sample file. This is a one-time operation
  # so reading a large file shouldn't be a problem
  file = File.read(Rails.root.join("db/seed_data/#{filename}.json"))
  # parse file to ruby hash. Expects to have a RECORDS array
  collection = JSON.parse(file)['RECORDS']
  # create an instance per each record in file
  puts "seeding #{filename}...."
  collection.each do |attrs|
    klass.create!(attrs)

  rescue ActiveRecord::RecordNotUnique
    # store next record if current already exists
    puts "#{klass.name} with id #{attrs['id']} already exists"
    next
  end
end
