# Calculates disbursements for merchants based on Order status
class Disburser

  def disburse_merchants
    # Creates a disbursement record for each merchant with
    # completed orders and complete such orders
    completed_merchant_orders.each do |merchant_id, total_amount|
      Disbursement.new.create_with(
        merchant: merchant_id,
        amount: total_amount
      )
    end
  end

  private

  def completed_merchant_orders
    # extracts total amount of completed orders, grouped by merchant
    completed_orders.group(:merchant_id)
                    .pluck('merchant_id, SUM(amount) as amount')
  end

  def completed_orders
    Order.completed
  end
end
