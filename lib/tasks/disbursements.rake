namespace :disbursements do

  desc 'Manually run disbursements for completed orders'
  task run: :environment do
    DisbursementsWorker.perform_async
    puts 'Enqueued Disbursements to process asynchronously'
  end

end
