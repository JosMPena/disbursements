require 'rails_helper'

RSpec.describe MerchantDisbursementsController, type: :controller do
  describe 'GET #index when records exist' do
    before do
      # setup test data
      FactoryBot.create_list :disbursement, 5
      FactoryBot.create(:disbursement, disbursed_on: Time.now + 3.months,
                                       merchant: Merchant.first)
      get :index, params: params
    end

    # week format is WW/YYYY, where W = week number and YYYY = year
    let(:week) { "#{Time.now.strftime('%W').to_i + 1}#{Time.now.strftime('/%Y')}" }

    context 'when week is in params and merchant is not' do
      let(:params) { { week: week } }

      it 'Should return records in the given week for all merchants' do
        expect(json.size).to eq 5
      end
    end

    context 'when merchant is in params and week is not' do
      let(:params) { { merchant: 1 } }

      it 'Should return records for the given merchant in any time' do
        expect(json.size).to eq 2
        ids = json.map { |r| r['merchant_id'] }.uniq
        expect(ids.size).to eq 1
      end
    end

    context 'when merchant and week are in params' do
      let(:params) { { merchant: 1, week: week } }

      it 'Should return records for the given merchant in the given week' do
        expect(json.size).to eq 1
        expect(json.last['merchant_id']).to eq 1
      end
    end

    context 'when merchant and week are not in params' do
      let(:params) { nil }
      it 'Should return all records' do
        expect(json.size).to eq Disbursement.count
      end
    end

    context 'when week param is incorrect' do
      let(:params) { { week: 'something' } }

      it 'should return expected message' do
        expect(json['message']).to eq 'Unsupported value for week: something. Correct format is WW/YYYY'
        expect(response.status).to eq 422
      end
    end

    context 'when merchant param is incorrect' do
      let(:params) { { merchant: 'zz' } }
      it 'should return expected message' do
        expect(json['message']).to eq 'nothing found'
        expect(response.status).to eq 404
      end
    end
  end

  describe 'GET #index when no record exist' do

    it 'should return not_found' do
      get :index

      expect(json['message']).to match('nothing found')
      expect(response.status).to eq 404
    end
  end
end
