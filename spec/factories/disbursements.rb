FactoryBot.define do
  factory :disbursement do
    association :merchant
    disbursed_amount { 10.99 }
    fee_amount { 0.1 }
    disbursed_on { Time.zone.now }
  end
end
