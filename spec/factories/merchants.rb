# Creates Merchant objects for testing
FactoryBot.define do
  factory :merchant do
    name { 'merchant 1' }
    email { 'mail@example.com' }
    cif { '11122233345' }
  end
end
