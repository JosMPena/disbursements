# Creates Order objects for testing
FactoryBot.define do
  factory :order do
    association :merchant
    association :shopper
    amount { 10.99 }
    completed_at { Time.zone.now }

    trait :pending do
      completed_at { nil }
    end
  end
end
