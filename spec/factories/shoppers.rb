# Creates Shopper objects for testing
FactoryBot.define do
  factory :shopper do
    name { 'shopper 1' }
    email { 'example@email.com' }
    nif { '09876543W' }
  end
end
