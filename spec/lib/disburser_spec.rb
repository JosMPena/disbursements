require 'rails_helper'

describe Disburser do
  subject { Disburser.new }

  describe '#disburse_merchants' do

    context 'when completed orders for a Merchant exist' do
      before { FactoryBot.create_list :order, 3 }

      it 'should create disbursements' do
        expect { subject.disburse_merchants }
          .to change(Disbursement, :count).from(0).to(3)
      end
    end

    context 'when completed orders for multiple Merchants exist' do
      before do
        merchant1, merchant2 = FactoryBot.build_list :merchant, 2
        FactoryBot.create_list :order, 2, merchant: merchant1
        FactoryBot.create_list :order, 3, merchant: merchant2
      end

      it 'should create only 1 disbursement per merchant' do
        expect { subject.disburse_merchants }
          .to change(Disbursement, :count).from(0).to(2)
      end

      it 'should disburse the total amount for the same merchant' do
        subject.disburse_merchants
        expect(Disbursement.last.disbursed_amount)
          .to be > FactoryBot.attributes_for(:order).dig(:amount)
      end
    end

    context 'when no completed orders exist' do
      before { FactoryBot.create_list :order, 3, :pending }

      it 'should not create disbursements' do
        expect { subject.disburse_merchants }.not_to change(Disbursement, :count)
      end
    end
  end
end
