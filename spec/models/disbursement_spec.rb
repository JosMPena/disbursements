require 'rails_helper'

RSpec.describe Disbursement, type: :model do
  describe 'associations' do
    it { expect(subject).to belong_to(:merchant) }
  end

  describe '#create_with' do
    let(:merchant) { FactoryBot.create :merchant }
    let(:record) { { amount: amount, merchant: merchant.id } }

    context 'when amount is not an expected value' do
      let(:amount) { -1 }
      it 'should raise an error' do
        expect { subject.create_with(record) }
          .to raise_error StandardError, '-1 is not a supported value'
      end
    end

    context 'when amount is an expected value' do
      let(:amount) { 100 }
      it 'should create a disbursement for the Merchant' do
        expect { subject.create_with(record) }.to change(Disbursement, :count).to(1)
      end
    end

    context 'when merchant amount is < 50' do
      let(:amount) { 49.99 }
      it 'should charge 1% fee over disbursement amount' do
        disbursement = subject.create_with(record)
        fee = amount * 0.01

        shared_expectations(disbursement, fee)
      end
    end

    context 'when merchant amount is >= 50 and <= 300' do
      let(:amount) { 123.45 }
      it 'should charge 0.95% fee over disbursement amount' do
        disbursement = subject.create_with(record)
        fee = amount * 0.0095

        shared_expectations(disbursement, fee)
      end
    end

    context 'when merchant amount is > 300' do
      let(:amount) { 300.01 }
      it 'should charge 0.85% fee over disbursement amount' do
        disbursement = subject.create_with(record)
        fee = amount * 0.0085

        shared_expectations(disbursement, fee)
      end
    end

    def shared_expectations(disbursement, fee)
      expect(disbursement.disbursed_amount).to eq(amount - fee)
      expect(disbursement.fee_amount).to eq fee
    end
  end
end
