require 'rails_helper'

RSpec.describe Merchant, type: :model do
  describe 'attributes' do
    it 'should have attribute name' do
      expect(subject).to respond_to(:name)
    end

    it 'should have email attribute' do
      expect(subject).to respond_to(:email)
    end

    it 'should have cif attribute' do
      expect(subject).to respond_to(:cif)
    end
  end

  describe 'associations' do
    it 'should have many orders' do
      expect(subject).to have_many :orders
    end
  end
end
