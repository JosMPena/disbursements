require 'rails_helper'

RSpec.describe Order, type: :model do

  describe 'class methods' do
    describe '#completed' do
      let!(:pending_orders) { FactoryBot.create_list(:order, 5, :pending) }

      context 'when there are completed and completed orders in DB' do
        let!(:completed_orders) { FactoryBot.create_list(:order, 5) }

        it 'should return completed orders only' do
          expect(Order.completed.to_a).to eq completed_orders
        end
      end

      context 'when there are not completed orders in DB' do
        it 'should return an empty relation' do
          expect(Order.completed.any?).to be_falsey
        end
      end
    end
  end
end
