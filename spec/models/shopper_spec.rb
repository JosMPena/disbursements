require 'rails_helper'

RSpec.describe Shopper, type: :model do
  describe 'attributes' do
    it 'should have an attribute name' do
      expect(subject).to respond_to(:name)
    end

    it 'should have an attribute email' do
      expect(subject).to respond_to(:email)
    end

    it 'should have an attribute nif' do
      expect(subject).to respond_to(:nif)
    end
  end

  describe 'associations' do
    it 'should have many orders' do
      expect(subject).to have_many :orders
    end
  end
end
