require 'rails_helper'
require 'sidekiq/testing'
Sidekiq::Testing.fake!

RSpec.describe DisbursementsWorker, type: :worker do
  it 'Should enqueue jobs in the default queue' do
    described_class.perform_async
    expect(described_class.queue).to eq 'default'
  end

  it 'Should add the job to the queue array' do
    expect { described_class.perform_async }
      .to change(described_class.jobs, :size).by(1)
    described_class.new.perform
  end
end
